import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateNewsDto } from './dto/create-news.dto';
import { UpdateNewsDto } from './dto/update-news.dto';
import { Repository } from 'typeorm';
import { News } from './entities/news.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class NewsService {
  constructor(
    @InjectRepository(News)
    private readonly newsRepository: Repository<News>,
  ) {}
  async create(createNewsDto: CreateNewsDto) {
    const news = this.newsRepository.create(createNewsDto);
    return await this.newsRepository.save(news);
  }

  async findAll() {
    return await this.newsRepository.find();
  }

  async findOne(id: number) {
    return await this.newsRepository.findOne({ where: { id } });
  }

  async update(id: number, updateNewsDto: UpdateNewsDto) {
    const news = await this.findOne(id);
    if (!news) {
      throw new NotFoundException();
    }
    Object.assign(news, updateNewsDto);
    return await this.newsRepository.save(news);
  }

  async remove(id: number) {
    const news = await this.findOne(id);
    if (!news) {
      throw new NotFoundException();
    }
    return await this.newsRepository.remove(news);
  }
}
