export class CreateNewsDto {
  name: string;

  place: string;

  author: string;

  description: string;

  active: boolean;
}
